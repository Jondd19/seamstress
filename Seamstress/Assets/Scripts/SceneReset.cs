﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SceneReset : MonoBehaviour
{
    int scenenum;
    public Image black;
    public Animator anim;
    void Start()
    {
       scenenum = UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex;
    }

    // Update is called once per frame
    void Update()
    {


        if (Input.GetKeyDown(KeyCode.R))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(scenenum);
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            StartCoroutine(Fading());
            UnityEngine.SceneManagement.SceneManager.LoadScene(scenenum+1);
        }
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(scenenum-1);
        }
		if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    IEnumerator Fading()
    {
        anim.SetBool("Fade", true);
        yield return new WaitUntil(() => black.color.a == 1);
    }

}
